/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_evalexp_main.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anmiron <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 17:09:36 by anmiron           #+#    #+#             */
/*   Updated: 2016/07/23 17:39:28 by anmiron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fct.h"

char    *g_p;
char    *g_str;

int     eval(void);

int     eval3(void)
{
	int l;

	l = 0;
	if (*g_p == '(')
	{
		g_p++;
		l = eval();
		g_p = g_p + 2;
	}
	else
	{
		while (*g_p >= '0' && *g_p <= '9')
		{
			l = l * 10 + *g_p - '0';
			g_p++;
		}
		if (*g_p == ' ')
			g_p++;
	}
	return (l);
}

int     eval2(void)
{
	int l;

	l = eval3();
	while (*g_p == '*' || *g_p == '/' || *g_p == '%')
	{
		if (*g_p == '*')
		{
			g_p = g_p + 2;
			l *= eval3();
		}
		if (*g_p == '/')
		{
			g_p = g_p + 2;
			l /= eval3();
		}
		if (*g_p == '%')
		{
			g_p = g_p + 2;
			l = l % eval3();
		}
	}
	return (l);
}

int     eval(void)
{
	int l;

	l = eval2();
	while (*g_p == '+' || *g_p == '-')
	{
		if (*g_p == '+')
		{
			g_p = g_p + 2;
			l += eval2();
		}
		if (*g_p == '-')
		{
			g_p = g_p + 2;
			l -= eval2();
		}
	}
	return (l);
}

int     eval_expr(char *str)
{
	int index;
	int val;

	index = 0;
	while (str[index])
		index++;
	g_str = (char *)malloc(index + 1);
	while (index)
	{
		index--;
		g_str[index] = str[index];
	}
	g_p = g_str;
	val = eval();
	return (val);
}

int     main(int ac, char **av)
{
	if (ac > 1)
	{
		ft_putnbr(eval_expr(av[1]));
		ft_putchar('\n');
	}
	return (0);
}
