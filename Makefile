# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: anmiron <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/23 16:37:57 by anmiron           #+#    #+#              #
#    Updated: 2016/07/23 17:46:58 by anmiron          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NUME = eval_expr

SRS = eval_expr.c ft_put.c

all: $(NUME)

$(NUME):
	gcc -o $(NUME) -Wall -Wextra -Werror $(SRS)

clean:
	/bin/rm -f *.o

fclean: clean
	/bin/rm -f $(NUME)

re: fclean all
