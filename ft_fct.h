/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anmiron <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 17:29:25 by anmiron           #+#    #+#             */
/*   Updated: 2016/07/23 17:35:11 by anmiron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FCT_H
# define FT_FCT_H

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>

void	ft_putchar(char c);
void	ft_putnbr(int nb);

#endif
